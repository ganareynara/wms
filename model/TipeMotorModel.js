const conn = require('../config/db');
const { responseData, responseMessage } = require('../config/response_handler');

exports.insertTipeMotor = (response, statement, data) => {

    try {
        // jalankan query
        conn.query(statement, data, (err, rows, field) => {
            // error handling
            if (err) {
                return response.status(500).json({ message: 'Gagal insert data!', error: err });
            }

            // jika request berhasil
            responseData(response, 201, data, 'Berhasil insert data!');
        });
    } catch (err) {
        return response.status(500).json({ message: 'Gagal insert data!', error: err });
    }

};

exports.getTipeMotor = (response, statement) => {

    // jalankan query
    conn.query(statement, (err, rows, field) => {
        // error handling
        if (err) {
            return response.status(500).json({ message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        responseData(response, 200, rows);
    });

};

exports.getTipeMotorById = (response, searchStatement, id) => {

    // jalankan query
    conn.query(searchStatement, id, (err, rows, field) => {
        // error handling
        if (err) {
            return response.status(500).json({ message: 'Ada kesalahan', error: err });
        }

        responseData(response, 200, rows);
    });

};

exports.updateTipeMotor = (response, searchStatement, updateStatement, id, data) => {
    
    try {
        // jalankan query untuk melakukan pencarian data
        conn.query(searchStatement, id, (err, rows, field) => {
            // error handling
            if (err) {
                return response.status(500).json({ message: 'Ada kesalahan', error: err });
            }
    
            // jika id yang dimasukkan sesuai dengan data yang ada di db
            if (rows.length) {
                // jalankan query update
                conn.query(updateStatement, [data, id], (err, rows, field) => {
                    // error handling
                    if (err) {
                        return response.status(500).json({ message: 'Ada kesalahan', error: err });
                    }
    
                    // jika update berhasil
                    responseData(response, 200, data, 'Berhasil update data!');
                });
            } else {
                return response.status(404).json({ message: 'Data tidak ditemukan!', success: false });
            }
        });
    } catch (err) {
        return response.status(500).json({ message: 'Gagal delete data!', error: err });
    }
};