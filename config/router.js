const express = require('express');
const routes  = express.Router();

const UserController                = require('../controller/UserController');
const GudangController              = require('../controller/GudangController');
const TipeMotorController           = require('../controller/TipeMotorController');
const MerkMotorController           = require('../controller/MerkMotorController');
const MerkSparepartController       = require('../controller/MerkSparePartController');
const JenisSparepartController      = require('../controller/JenisSparePartController');
const KategoriSparepartController   = require('../controller/KategoriSparePartController');

// pengguna
routes.post('/pengguna/insert', UserController.createData);
routes.get('/pengguna/list', UserController.readData);
routes.get('/pengguna/:id', UserController.readDataById);
routes.post('/pengguna/update/:id', UserController.updateData);
routes.post('/pengguna/delete/:id', UserController.deleteData);
// end pengguna

// gudang
routes.post('/gudang/insert', GudangController.createData);
routes.get('/gudang/list', GudangController.readData);
routes.get('/gudang/:id', GudangController.readDataById);
routes.post('/gudang/update/:id', GudangController.updateData);
routes.post('/gudang/delete/:id', GudangController.deleteData);
// gudang

// tipe motor
routes.post('/tipe_motor/insert', TipeMotorController.createData);
routes.get('/tipe_motor/list', TipeMotorController.readData);
routes.get('/tipe_motor/:id', TipeMotorController.readDataById);
routes.post('/tipe_motor/update/:id', TipeMotorController.updateData);
routes.post('/tipe_motor/delete/:id', TipeMotorController.deleteData);
// tipe motor

// merk motor
routes.post('/merk_motor/insert', MerkMotorController.createData);
routes.get('/merk_motor/list', MerkMotorController.readData);
routes.get('/merk_motor/:id', MerkMotorController.readDataById);
routes.post('/merk_motor/update/:id', MerkMotorController.updateData);
routes.post('/merk_motor/delete/:id', MerkMotorController.deleteData);
// merk motor

// merk sparepart
routes.post('/merk_sparepart/insert', MerkSparepartController.createData);
routes.get('/merk_sparepart/list', MerkSparepartController.readData);
routes.get('/merk_sparepart/:id', MerkSparepartController.readDataById);
routes.post('/merk_sparepart/update/:id', MerkSparepartController.updateData);
routes.post('/merk_sparepart/delete/:id', MerkSparepartController.deleteData);
// merk sparepart

// jenis sparepart
routes.post('/jenis_sparepart/insert', JenisSparepartController.createData);
routes.get('/jenis_sparepart/list', JenisSparepartController.readData);
routes.get('/jenis_sparepart/:id', JenisSparepartController.readDataById);
routes.post('/jenis_sparepart/update/:id', JenisSparepartController.updateData);
routes.post('/jenis_sparepart/delete/:id', JenisSparepartController.deleteData);
// jenis sparepart

// kategori sparepart
routes.post('/kategori_sparepart/insert', KategoriSparepartController.createData);
routes.get('/kategori_sparepart/list', KategoriSparepartController.readData);
routes.get('/kategori_sparepart/:id', KategoriSparepartController.readDataById);
routes.post('/kategori_sparepart/update/:id', KategoriSparepartController.updateData);
routes.post('/kategori_sparepart/delete/:id', KategoriSparepartController.deleteData);
// kategori sparepart

module.exports = routes;