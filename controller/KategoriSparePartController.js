const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var md5 = require('md5');

const {
    insertKategoriSparepart,
    getKategoriSparepart,
    getKategoriSparepartById,
    updateKategoriSparepart
} = require('../model/KategoriSparePartModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        nama_kategori,
        nama_lain_kategori
    } = req.body;

    const dataTipeKategoriSparepart = {
        id: uuidv4(),
        nama_kategori: nama_kategori,
        nama_lain_kategori: nama_lain_kategori,
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO master_kategori_sparepart SET ?';

    if (nama_kategori == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Kategori Sparepart Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertKategoriSparepart(res, querySql, dataTipeKategoriSparepart);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM master_kategori_sparepart WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getKategoriSparepart(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM master_kategori_sparepart WHERE id = ? AND deleted_at IS NULL';
    getKategoriSparepartById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        nama_kategori,
        nama_lain_kategori
    } = req.body;

    const dataTipeKategori = {
        nama_kategori: nama_kategori,
        nama_lain_kategori: nama_lain_kategori,
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM master_kategori_sparepart WHERE id = ?';
    const queryUpdate = 'UPDATE master_kategori_sparepart SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateKategoriSparepart(res, querySearch, queryUpdate, req.params.id, dataTipeKategori);
};

exports.deleteData = (req, res) => {

    const data = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM master_kategori_sparepart WHERE id = ?';
    const queryUpdate = 'UPDATE master_kategori_sparepart SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateKategoriSparepart(res, querySearch, queryUpdate, req.params.id, data);
};