const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var bcrypt = require("bcryptjs");

const {
    insertUser,
    getUsers,
    getUserById,
    updateUser
} = require('../model/UserModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        username,
        nama,
        posisi_gudang,
        jabatan,
        password
    } = req.body;

    const dataUser = {
        id: uuidv4(),
        username: username,
        nama: nama,
        posisi_gudang: posisi_gudang,
        jabatan: jabatan,
        password: bcrypt.hashSync(password, 8),
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO users SET ?';

    if (username == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Username Pegawai Tidak Boleh Kosong',
            "data": null
        });

    } else {
        const _isExist = 'SELECT * FROM users WHERE username = ? AND deleted_at IS NULL';
        const isExistgetUserData = getUserById(res, _isExist, dataUser.username);

        if (getUserData) {
            return res.status(500).send({
                "success": false,
                "message": 'Username Telah Digunakan',
                "data": null
            });
        }
    }

    if (nama == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Pegawai Tidak Boleh Kosong',
            "data": null
        });

    }

    if (posisi_gudang == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Lokasi Kerja Tidak Boleh Kosong',
            "data": null
        });

    }

    if (jabatan == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Jabatan Pekerja Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertUser(res, querySql, dataUser);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM users WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getUsers(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM users WHERE id = ? AND deleted_at IS NULL';
    getUserById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        username,
        nama,
        posisi_gudang,
        jabatan,
        password
    } = req.body;

    const dataUser = {
        username: username,
        nama: nama,
        posisi_gudang: posisi_gudang,
        jabatan: jabatan,
        password: md5(password),
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM users WHERE id = ?';
    const queryUpdate = 'UPDATE users SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateUser(res, querySearch, queryUpdate, req.params.id, dataUser);
};

exports.deleteData = (req, res) => {

    const dataUser = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM users WHERE id = ?';
    const queryUpdate = 'UPDATE users SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateUser(res, querySearch, queryUpdate, req.params.id, dataUser);
};