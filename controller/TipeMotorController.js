const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var md5 = require('md5');

const {
    insertTipeMotor,
    getTipeMotor,
    getTipeMotorById,
    updateTipeMotor
} = require('../model/TipeMotorModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        nama_tipe_motor,
        nama_lain_tipe_motor,
        tahun_tipe_motor,
        jenis_tipe_motor
    } = req.body;

    const dataTipeMotor = {
        id: uuidv4(),
        nama_tipe: nama_tipe_motor,
        nama_lain_tipe: nama_lain_tipe_motor,
        tahun_tipe: tahun_tipe_motor,
        jenis_tipe: jenis_tipe_motor,
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO master_tipe_motor SET ?';

    if (nama_tipe_motor == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Tipe Motor Tidak Boleh Kosong',
            "data": null
        });

    }

    if (tahun_tipe_motor == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Tahun Tipe Motor Tidak Boleh Kosong',
            "data": null
        });

    }

    if (jenis_tipe_motor == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Jenis Tipe Motor Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertTipeMotor(res, querySql, dataTipeMotor);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM master_tipe_motor WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getTipeMotor(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM master_tipe_motor WHERE id = ? AND deleted_at IS NULL';
    getTipeMotorById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        nama_tipe_motor,
        nama_lain_tipe_motor,
        tahun_tipe_motor,
        jenis_tipe_motor
    } = req.body;

    const dataTipeMotor = {
        nama_tipe: nama_tipe_motor,
        nama_lain_tipe: nama_lain_tipe_motor,
        tahun_tipe: tahun_tipe_motor,
        jenis_tipe: jenis_tipe_motor,
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM master_tipe_motor WHERE id = ?';
    const queryUpdate = 'UPDATE master_tipe_motor SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateTipeMotor(res, querySearch, queryUpdate, req.params.id, dataTipeMotor);
};

exports.deleteData = (req, res) => {

    const dataTipeMotor = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM master_tipe_motor WHERE id = ?';
    const queryUpdate = 'UPDATE master_tipe_motor SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateTipeMotor(res, querySearch, queryUpdate, req.params.id, dataTipeMotor);
};