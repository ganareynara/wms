const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var md5 = require('md5');

const {
    insertMerkSparepart,
    getMerkSparepart,
    getMerkSparepartById,
    updateMerkSparepart
} = require('../model/MerkSparePartModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        nama_merk,
        jenis_merk
    } = req.body;

    const dataTipeMerkSparepart = {
        id: uuidv4(),
        nama_merk: nama_merk,
        jenis_merk: jenis_merk,
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO master_merk_sparepart SET ?';

    if (nama_merk == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Merk Sparepart Tidak Boleh Kosong',
            "data": null
        });

    }

    if (jenis_merk == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Jenis Sparepart Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertMerkSparepart(res, querySql, dataTipeMerkSparepart);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM master_merk_sparepart WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getMerkSparepart(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM master_merk_sparepart WHERE id = ? AND deleted_at IS NULL';
    getMerkSparepartById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        nama_merk,
        jenis_merk
    } = req.body;

    const dataTipeMerk = {
        nama_merk: nama_merk,
        jenis_merk: jenis_merk,
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM master_merk_sparepart WHERE id = ?';
    const queryUpdate = 'UPDATE master_merk_sparepart SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateMerkSparepart(res, querySearch, queryUpdate, req.params.id, dataTipeMerk);
};

exports.deleteData = (req, res) => {

    const data = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM master_merk_sparepart WHERE id = ?';
    const queryUpdate = 'UPDATE master_merk_sparepart SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateMerkSparepart(res, querySearch, queryUpdate, req.params.id, data);
};