const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var md5 = require('md5');

const {
    insertJenisSparepart,
    getJenisSparepart,
    getJenisSparepartById,
    updateJenisSparepart
} = require('../model/JenisSparePartModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        nama_jenis,
        nama_lain_jenis
    } = req.body;

    const dataTipeJenisSparepart = {
        id: uuidv4(),
        nama_jenis: nama_jenis,
        nama_lain_jenis: nama_lain_jenis,
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO master_jenis_sparepart SET ?';

    if (nama_jenis == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Jenis Sparepart Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertJenisSparepart(res, querySql, dataTipeJenisSparepart);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM master_jenis_sparepart WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getJenisSparepart(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM master_jenis_sparepart WHERE id = ? AND deleted_at IS NULL';
    getJenisSparepartById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        nama_jenis,
        nama_lain_jenis
    } = req.body;

    const dataTipeJenis = {
        nama_jenis: nama_jenis,
        nama_lain_jenis: nama_lain_jenis,
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM master_jenis_sparepart WHERE id = ?';
    const queryUpdate = 'UPDATE master_jenis_sparepart SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateJenisSparepart(res, querySearch, queryUpdate, req.params.id, dataTipeJenis);
};

exports.deleteData = (req, res) => {

    const data = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM master_jenis_sparepart WHERE id = ?';
    const queryUpdate = 'UPDATE master_jenis_sparepart SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateJenisSparepart(res, querySearch, queryUpdate, req.params.id, data);
};