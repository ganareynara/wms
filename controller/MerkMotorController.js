const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var md5 = require('md5');

const {
    insertMerkMotor,
    getMerkMotor,
    getMerkMotorById,
    updateMerkMotor
} = require('../model/MerkMotorModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        nama_merk_motor,
        nama_lain_merk_motor
    } = req.body;

    const dataTipeMotor = {
        id: uuidv4(),
        nama_merk: nama_merk_motor,
        nama_lain_merk: nama_lain_merk_motor,
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO master_merk_motor SET ?';

    if (nama_merk_motor == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Merk Motor Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertMerkMotor(res, querySql, dataTipeMotor);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM master_merk_motor WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getMerkMotor(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM master_merk_motor WHERE id = ? AND deleted_at IS NULL';
    getMerkMotorById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        nama_merk_motor,
        nama_lain_merk_motor
    } = req.body;

    const dataTipeMotor = {
        nama_merk: nama_merk_motor,
        nama_lain_merk: nama_lain_merk_motor,
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM master_merk_motor WHERE id = ?';
    const queryUpdate = 'UPDATE master_merk_motor SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateMerkMotor(res, querySearch, queryUpdate, req.params.id, dataTipeMotor);
};

exports.deleteData = (req, res) => {

    const dataTipeMotor = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM master_merk_motor WHERE id = ?';
    const queryUpdate = 'UPDATE master_merk_motor SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateMerkMotor(res, querySearch, queryUpdate, req.params.id, dataTipeMotor);
};