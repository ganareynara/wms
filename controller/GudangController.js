const conn = require('../config/db');
const { v4: uuidv4 } = require('uuid');
var md5 = require('md5');

const {
    insertGudang,
    getGudang,
    getGudangById,
    updateGudang
} = require('../model/GudangModel');

exports.createData = (req, res) => {
    // buat variabel penampung data dan query sql
    const {
        nama_gudang,
        alamat_gudang,
        urutan_gudang
    } = req.body;

    const dataGudang = {
        id: uuidv4(),
        nama_gudang: nama_gudang,
        alamat_gudang: alamat_gudang,
        urutan_gudang: urutan_gudang,
        created_at: new Date(),
        created_by: 'gana',
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySql = 'INSERT INTO master_gudang SET ?';

    if (nama_gudang == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Nama Gudang Tidak Boleh Kosong',
            "data": null
        });

    }

    if (alamat_gudang == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Alamat Gudang Tidak Boleh Kosong',
            "data": null
        });

    }

    if (urutan_gudang == null) {

        return res.status(500).send({
            "success": false,
            "message": 'Urutan Gudang Tidak Boleh Kosong',
            "data": null
        });

    }

    // masukkan ke dalam model
    insertGudang(res, querySql, dataGudang);
    // insertBootcamp(res, querySql, data);
};

exports.readData = (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM master_gudang WHERE deleted_at IS NULL';

    // masukkan ke dalam model
    getGudang(res, querySql);
};

exports.readDataById = (req, res) => {
    const querySearch = 'SELECT * FROM master_gudang WHERE id = ? AND deleted_at IS NULL';
    getGudangById(res, querySearch, req.params.id);
};

exports.updateData = (req, res) => {

    const {
        nama_gudang,
        alamat_gudang,
        urutan_gudang
    } = req.body;

    const dataGudang = {
        nama_gudang: nama_gudang,
        alamat_gudang: alamat_gudang,
        urutan_gudang: urutan_gudang,
        updated_at: new Date(),
        updated_by: 'gana'
    };

    const querySearch = 'SELECT * FROM master_gudang WHERE id = ?';
    const queryUpdate = 'UPDATE master_gudang SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateGudang(res, querySearch, queryUpdate, req.params.id, dataGudang);
};

exports.deleteData = (req, res) => {

    const dataGudang = {
        deleted_at  : new Date(),
        deleted_by  : 'gana'
    };

    const querySearch = 'SELECT * FROM master_gudang WHERE id = ?';
    const queryUpdate = 'UPDATE master_gudang SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateGudang(res, querySearch, queryUpdate, req.params.id, dataGudang);
};